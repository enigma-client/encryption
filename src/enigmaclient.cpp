/* src/enigmaclient.cpp */

// include files from Qt
#include <QtNetwork>
#include <QtWidgets>
#include <QMessageBox>
#include <QBoxLayout>

// include files from project
#include "inc/enkinterface.h"
#include "src/enigmaclient.h"

#define RELEASE_VERSION "1.0.0"

EnigmaClient::EnigmaClient()
{
    // set focus policy to trigger QMainWindow SIGNAL(focusChanged(QWidget*, QWidget* )))
    setFocusPolicy(Qt::StrongFocus);
    // set main window minimum size
    setMinimumSize(480,120);
    QWidget* centreWidget = new QWidget(this);
    // widgets to display encrypted and decrypted messages
    txtDecodedMessage_m = new QTextEdit("Enter a message here.", this);
    txtEncodedMessage_m = new QTextEdit("View of encrypted message.");
    lblEncodedSize_m = new QLabel("Encrupted Size:");
    cmdEncryptMessage_m = new QPushButton("Encrypt Message", this);
    cmdDecryptMessage_m = new QPushButton("Decrypt Message", this);
    encryptControlsLayout_m = new QBoxLayout(QBoxLayout::TopToBottom);
    encryptControlsLayout_m->addWidget(cmdEncryptMessage_m);

    decryptControlsLayout_m = new QBoxLayout(QBoxLayout::TopToBottom);
    decryptControlsLayout_m->addWidget(cmdDecryptMessage_m);
    decryptControlsLayout_m->addWidget(lblEncodedSize_m);

    QGridLayout* mainLayout = new QGridLayout;
    mainLayout->addWidget(txtDecodedMessage_m,0,0);
    mainLayout->addLayout(encryptControlsLayout_m, 0 ,1);
    mainLayout->addWidget(txtEncodedMessage_m,1,0);
    mainLayout->addLayout(decryptControlsLayout_m, 1 ,1);

    //mainLayout->addWidget(cmdDecryptMessage_m,1,1);
    // set new grid layout as layout for centralWidget
    centreWidget->setLayout(mainLayout);
    setCentralWidget(centreWidget);
    // use status bar to display enk connection status
    createStatusBar();

    connect(cmdEncryptMessage_m, SIGNAL(clicked()), this, SLOT(slotEncryptMessage()));
    connect(cmdDecryptMessage_m, SIGNAL(clicked()), this, SLOT(slotDecryptMessage()));

    /******************************************************************************/
    /*********************** DO NOT CHANGE THIS CODE BELOW ************************/
    /******************************************************************************/
    // create new instance of EnkInterface ( supplied in DLL wrapper )
    enkInterface_m = new EnkInterface(this);
    // connect all the signals from the EnkInterface to slot routines ( Qt type of callback function )
    connect(enkInterface_m, SIGNAL(connected()), this, SLOT(slotServerConnected()));
    connect(enkInterface_m, SIGNAL(disconnected()), this, SLOT(slotServerDisconnected()));
    connect(enkInterface_m, SIGNAL(deactivated()), this, SLOT(slotEnkDeactivated()));
    connect(enkInterface_m, SIGNAL(inserted()), this, SLOT(slotEnkInserted()));
    connect(enkInterface_m, SIGNAL(reactivated()), this, SLOT(slotEnkReactivated()));
    connect(enkInterface_m, SIGNAL(removed()), this, SLOT(slotEnkRemoved()));
    connect(enkInterface_m, SIGNAL(connectionError(int)), this, SLOT(slotServerError(int)));
    // connect application focus changed signal to slot routine
    connect(qApp, SIGNAL(focusChanged(QWidget*, QWidget* )), this, SLOT(slotFocusChange(QWidget*, QWidget*)) );
    /******************************************************************************/
    /**************************** END OF CODE BLOCK *******************************/
    /******************************************************************************/
}

EnigmaClient::~EnigmaClient()
{
    // no need to delete child widgets, Qt does it all for us
}

void EnigmaClient::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}

/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::init()
{
    // set startup flag so as to show ENK connection error in slotServerError()
    startup_m = true;
    // initialse ENK Interface and connect to EnigmaServer
    enkInterface_m->initENKServer();
    // following is necessary because signals are not being sent by QApplication at this point
    setWindowTitle("Enigma Client-Base "  + QString(RELEASE_VERSION) + "       No Server Connection");
    // if succesfully connected to EnigmaServer show some information

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code
    if(enkInterface_m->isConnected())
    {
        if(enkInterface_m->enkIsInserted())
        {
            int version = enkInterface_m->getVersion();
            setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       ENK v" + QString("").setNum((float)version / 10));
            if(enkInterface_m->enkIsActive())
            {
                statusBar()->showMessage("ENK activated.");
            }
            else
            {
                statusBar()->showMessage("ENK deactivated.");
            }
         }
         else
         {
            setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       No ENK Connection");
            statusBar()->showMessage("Please insert and activate your ENK.");
         }
    }
    else
    {
        setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       No Connection");
        statusBar()->showMessage("Please restart your enigma server.");
    }
    // end of user specific code
    /******************************************************************************/

    // remove startup flag
    startup_m = false;
}

/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotServerConnected()
{
    // triggered by signal connected() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    if(enkInterface_m->enkIsInserted())
    {
        int version = enkInterface_m->getVersion();
        setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       ENK v" + QString("").setNum((float)version / 10));
        if(enkInterface_m->enkIsActive())
        {
            statusBar()->showMessage("ENK activated.");
        }
        else
        {
            statusBar()->showMessage("ENK deactivated.");
        }
     }
     else
     {
        setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       No ENK Connection");
        statusBar()->showMessage("Please insert and activate your ENK.");
     }

    // end of user specific code
    /******************************************************************************/

}

/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotServerDisconnected()
{
    // triggered by signal disconnected() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       No Connection");
    statusBar()->showMessage("Please restart your enigma server.");

    // end of user specific code
    /******************************************************************************/

}

/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotServerError(int socketError)
{
    // triggered by signal connectionError() from ENKInterface
    QString connectionError;
    switch (socketError)
    {
        case QLocalSocket::ServerNotFoundError:
            if(startup_m)
                connectionError = (tr("The enigma server was not found.\n\n"
                                                    "Please restart your enigma server."));
            else
                connectionError = QString("");
         break;
        case QLocalSocket::ConnectionRefusedError:
            if(startup_m)
            connectionError = (tr("The connection was refused by the server.\n\n"
                                     "Please make sure that your enigma server is running."));
            else
                connectionError = QString("");
            break;
        case QLocalSocket::PeerClosedError:
         connectionError = (tr("The enigma server closed the connection.\n\n"
                                     "Please restart your enigma server."));
         break;
        default:
         connectionError = (tr("The following error occurred: %1.").arg(socketError));
    }
    if(connectionError != QString(""))
    {
        // important to disconnect focusChanged signal when displaying another QDialog window (QMessageBox)
        disconnect(qApp, SIGNAL(focusChanged(QWidget*, QWidget* )), this, SLOT(slotFocusChange(QWidget*, QWidget*)) );
        QMessageBox::critical(this, "EnigmaClient Connection Error", connectionError);
        connect(qApp, SIGNAL(focusChanged(QWidget*, QWidget* )), this, SLOT(slotFocusChange(QWidget*, QWidget*)) );
    }
}

/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotEnkDeactivated()
{
    // triggered by signal deactivated() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    statusBar()->showMessage("Enk deactivated.");

    // end of user specific code
    /******************************************************************************/

}
/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotEnkReactivated()
{
    // triggered by signal reactivated() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code

    statusBar()->showMessage("Enk activated.");

    // end of user specific code
    /******************************************************************************/

}
/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotEnkInserted()
{
    // triggered by signal reactivated() from ENKInterface
    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code
    int version = enkInterface_m->getVersion();
    setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       ENK v" + QString("").setNum((float)version / 10));
    statusBar()->showMessage("Enk deactivated.");

    // end of user specific code
    /******************************************************************************/

}
/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotEnkRemoved()
{
    // triggered by signal reactivated() from ENKInterface

    /******************************************************************************/
    // following code is user specific and can be removed and replaced with new code
    setWindowTitle("Enigma Client - Encryption "  + QString(RELEASE_VERSION) + "       No ENK Connection");
    statusBar()->showMessage("Please insert and activate your ENK.");

    // end of user specific code
    /******************************************************************************/

}

/******************************************************************************/
/************************ DO NOT REMOVE THIS FUNCTION *************************/
/******************************************************************************/
void EnigmaClient::slotFocusChange(QWidget* oldW, QWidget* newW)
{
    // triggered by signal focusChanged() from QApplication
    // check if application getting focus
    // oldW == 0 then app is receiving focus from outside of app window
    if(oldW == 0)
    {
        enkInterface_m->refresh();
    }
}

void EnigmaClient::slotEncryptMessage()
{
    QFont fixedFont = QFont("Courier");
    fixedFont.setStyleHint(QFont::Monospace);
    fixedFont.setPointSize(8);
    fixedFont.setFixedPitch(true);

    txtEncodedMessage_m->setCurrentFont(fixedFont);

    if(txtDecodedMessage_m->toPlainText() != QString(""))
    {
        // call to ENK interface to return encrypted data as QByteArray
        // call requires
        // QByteArray data (data to be encrypted)
        // int ehtId ( eht index to use for encryption 0 to max_eht)

        encryptedDataBuffer_m = enkInterface_m->encryptData(txtDecodedMessage_m->toPlainText().toUtf8(), 0);
        txtEncodedMessage_m->setText(QString(encryptedDataBuffer_m.toHex()));
        lblEncodedSize_m->setText(QString("").setNum(encryptedDataBuffer_m.size()) + " bytes");
    }
}

void EnigmaClient::slotDecryptMessage()
{
    if(txtEncodedMessage_m->toPlainText() != QString(""))
    {
        // call to ENK interface to return decrypted data as QByteArray
        // call requires
        // QByteArray encrypted_data (data to be decrypted)
        // ( eht index used for decryption is selected by ENK )

        QByteArray decryptedData = enkInterface_m->decryptData(encryptedDataBuffer_m);
        QString decryptedMessage(decryptedData);
        //txtDecodedMessage_m->setText(decryptedMessage);
        QMessageBox::information(this, "Decrypted Message", decryptedMessage);
    }
}

//QString EnigmaClient::format(QByteArray byteArray)
//{

//}

QString EnigmaClient::printHex(QByteArray byteArray)
{
    unsigned int n;
    int column;
    int maxColumns = 8;
    QString hexOutput;

    const char* charArray = byteArray.constData();
    column = 0;

    for (n = 0; n < strlen(charArray); n++)
    {
        column++;
        unsigned char hexNum = (unsigned char)charArray[n];
        unsigned char hicode = (unsigned char)(hexNum / 16);
        unsigned char lowcode = hexNum - (hicode * 16);
        if(hicode > 9)
            hicode = hicode + 55;
        else
            hicode = hicode + 48;
        if(lowcode > 9)
            lowcode = lowcode + 55;
        else
            lowcode = lowcode + 48;
            hexOutput.append("0x" + QString(QChar(hicode)) + QString(QChar(lowcode)) + "  ");
        if(column == maxColumns)
        {
            hexOutput.append("\n");
            column = 0;
        }
    }
    return hexOutput;
}
