/* src/enigmaclient.h */

#ifndef ENIGMACLIENT_H
#define ENIGMACLIENT_H

#include <QMainWindow>

class EnkInterface;
class QBoxLayout;
class QTextEdit;
class QLabel;
class QPushButton;

class EnigmaClient : public QMainWindow
{
    Q_OBJECT
public:
    EnigmaClient();
    virtual ~EnigmaClient();
    void init();

private:
    void createStatusBar();
    QString printHex(QByteArray byteArray);
private:
    QBoxLayout* encryptControlsLayout_m;
    QBoxLayout* decryptControlsLayout_m;
    QTextEdit* txtDecodedMessage_m;
    QTextEdit* txtEncodedMessage_m;
    QLabel* lblEncodedSize_m;
    QPushButton* cmdEncryptMessage_m;
    QPushButton* cmdDecryptMessage_m;

    EnkInterface* enkInterface_m;
    bool startup_m;
    QByteArray encryptedDataBuffer_m;

public slots:
    void slotServerConnected();
    void slotServerDisconnected();
    void slotServerError(int socketError);
    void slotEnkDeactivated();
    void slotEnkInserted();
    void slotEnkReactivated();
    void slotEnkRemoved();
    void slotFocusChange(QWidget* oldW, QWidget* newW);

    void slotDecryptMessage();
    void slotEncryptMessage();
};
#endif // ENIGMACLIENT
