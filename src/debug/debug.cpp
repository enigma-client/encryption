/****************************************************************************
** Form implementation generated from reading ui file 'prog/util/debug.ui'
**
** Created: Wed Oct 4 07:47:31 2006
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.3.3   edited Nov 24 2003 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/
#include "debug.h"


//#include <qvariant.h>
//#include <qstring.h>
#include <qlayout.h>
//#include <qtooltip.h>
//#include <qwhatsthis.h>
//#include <qimage.h>
//#include <qpixmap.h>


/*
 *  Constructs a Debug as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 */
Debug::Debug( QWidget* parent, Qt::WindowFlags fl )
    : QWidget( parent, fl )
{
	QVBoxLayout *layout = new QVBoxLayout();
	txtDebug = new CDebugIO(this);
	layout->addWidget(txtDebug);
	setLayout(layout);
	setWindowTitle( "Debug" );
	setVisible(false);
	//setSizePolicy( QSizePolicy( (QSizePolicy::SizeType)3, (QSizePolicy::SizeType)3, 0, 0, sizePolicy().hasHeightForWidth() ) );
	languageChange();
	//resize(QSize(1600, 200));
	//clearWState( WState_Polished );
	//init();
}

/*
 *  Destroys the object and frees any allocated resources
 */
Debug::~Debug()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void Debug::languageChange()
{
	setWindowTitle( tr( "Debug Output" ) );
}

void Debug::print(const char * message)
{
		txtDebug->append(QString(message));
}

void Debug::print(QString message)
{
	 txtDebug->append(message);
}
