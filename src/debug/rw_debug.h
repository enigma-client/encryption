#ifndef RW_DEBUG_H
#define RW_DEBUG_H

#define DEBUG RW_Debug::instance()

#include "debug.h"

#include <qstring.h>
#include <QTime>
#include <windows.h>

typedef struct{
	LARGE_INTEGER start;
	LARGE_INTEGER inter;
	LARGE_INTEGER stop;
} RW_PerformanceTimer;

class RW_Debug
{

public:
    /**
     * Enum for debug levels. Only messages of the current
     * or a higher level are printed.
     * <ul>
     *  <li>D_NOTHING:  nothing
     *  <li>D_CRITICAL: critical messages
     *  <li>D_ERROR:    errors
     *  <li>D_WARNING:  warnings
     *  <li>D_NOTICE:   notes
     *  <li>D_INFORMATIONAL: infos
     *  <li>D_DEBUGGING: very verbose
     * </ul>
     */
    enum RW_DebugLevel { D_NOTHING,
                         D_CRITICAL,
                         D_ERROR,
                         D_WARNING,
                         D_NOTICE,
                         D_INFORMATIONAL,
                         D_DEBUGGING };

private:
	RW_Debug();
	Debug* panel;
	QTime timer1;
	QTime timer2;
public:
	static RW_Debug* instance();

	static void deleteInstance();
 	void off();
	void on();   
	void show();
	void setPos(int x, int y, int w, int h);
	//void setLevel(RW_DebugLevel level);
	//RW_DebugLevel getLevel();
	//void print(RW_DebugLevel level, const char* format ...);
	bool runAborted();
	//bool RUNX();
	void stop();
	void print(const char* format ...);
	void print(QString format);
	void printHex(char* string, int length);
	QString toHex(char* string, int length);
	int readTimer1_msecs();
	int readTimer1_secs();
	void setTimer1();
	void startTimer1();
	//void stopTimer1();
	void printTimer1Elapsed(QString info);
	int readTimer2_msecs();
	int readTimer2_secs();
	void setTimer2();
	void startTimer2();
	void printTimer2Elapsed(QString info);

	void printPerformanceTimer1_secs(QString info);
	void printPerformanceTimer1_msecs(QString info);
	void printPerformanceTimer1Elapsed(QString info);
	int readPerformanceTimer1_msecs();
	//void runX(bool state);
	//void setPerformanceTimer1();
	void startPerformanceTimer1();
	void stopPerformanceTimer1();

	void printPerformanceTimer2_msecs(QString info);
	int readPerformanceTimer2_msecs();
	void startPerformanceTimer2();
	void stopPerformanceTimer2();
    //void printUnicode(const RW_String& text);
    //void timestamp();
    //void setStream(FILE* s) {
        //stream = s;
    //}

private:
    static RW_Debug* uniqueInstance;
	bool on_m;
	//bool runX_m;
	bool aborted_m;
	int lastTime_m;

	RW_PerformanceTimer performanceTimer1_m;
	RW_PerformanceTimer performanceTimer2_m;
	LARGE_INTEGER performanceFrequency_m;
    //RS_DebugLevel debugLevel;
    //FILE* stream;
};

#endif

// EOF
