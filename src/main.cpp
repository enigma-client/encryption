/* src/main.cpp */

// include files from Qt
#include <QApplication>
#include <QtWidgets>
// include files from project
#include "src/enigmaclient.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    EnigmaClient client;
    client.resize(350,100);
    client.init();
    client.show();
    return app.exec();
}
